### 目录说明

- cloudfunctions：源开发环境目录，文件夹下包含所有的云函数

- miniprogram：项目目录

  - miniprogram_npm：引入的组件目录。包含【vant-weapp、iview-weapp】
  - pages：项目页面目录
    - index：首页
    - addMemo：添加备忘
    - editMemo：修改备忘
    - searchMemo：搜索备忘
    - my：个人中心
    - about：关于我们

  - statis：静态资源目录
  - utils：各类工具目录