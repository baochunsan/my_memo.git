const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


/**
 * 获取日期
 * @param {*} date 
 */
const getDateTime = date => {
  const year = date.getFullYear()
  var yy = year.toString().substr(2, 2);
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  return yy + '年' + month + '月' + day + '日' + ' ' + [hour, minute].map(formatNumber).join(':')
}


/**
 * 复制内容
 */
const copyContent = content => {
  wx.setClipboardData({
    data: content,
    success: function (res) {

    }
  });
}



// 展示加载框
function showLoading(message) {
  if (wx.showLoading) {
    // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.showLoading({
      title: message,
      mask: true
    });
  } else {
    // 低版本采用Toast兼容处理并将时间设为20秒以免自动消失
    wx.showToast({
      title: message,
      icon: 'loading',
      mask: true,
      duration: 20000
    });
  }
}

//隐藏加载框
function hideLoading() {
  if (wx.hideLoading) {
    // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.hideLoading();
  } else {
    wx.hideToast();
  }
}

module.exports = {
  formatTime: formatTime,
  copyContent: copyContent,
  getDateTime: getDateTime,
  showLoading: showLoading,
  hideLoading: hideLoading
}
