// pages/addMemo/index.js
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
const utils = require("../../utils/util");
const db = wx.cloud.database();
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 优先级选择项目
    levelColumns: ['无', '低', '中', '高'],
    title: '',
    content: '',
    levelNum: 0,
    levelName: '无',
    levelShow: false,
    levelDirection: '', //箭头展示位置
    status: 0,
    statusChecked: false,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    // 设置分享标题
    return {
      title: app.globalData.shareTitle,
      path: '/pages/index/index'
    }
  },

  /**
   * 标题输入监听
   */
  onChangeTitle: function ({
    detail
  }) {
    var that = this;
    that.setData({
      title: detail
    })
  },

  /**
   * 点击选择优先级
   */
  onClickLevel: function () {
    var that = this;
    that.setData({
      levelShow: true,
      levelDirection: 'down'
    })
  },

  /**
   * 关闭弹框监听事件
   */
  onCloseLevel: function () {
    var that = this;
    that.setData({
      levelShow: false,
      levelDirection: ''
    })
  },

  /**
   * 选择事件
   */
  onConfirmLevel: function ({
    detail
  }) {
    var that = this;
    var index = detail.index;
    var value = detail.value;
    that.setData({
      levelNum: index,
      levelName: value,
      levelShow: false,
      levelDirection: ''
    })
  },

  /**
   * 内容输入监听
   */
  onChangeContent: function ({
    detail
  }) {
    var that = this;
    that.setData({
      content: detail
    })
  },

  /**
   * 是否完成选择监听
   */
  onChangeStatus: function ({
    detail
  }) {
    var that = this;
    if (detail) {
      that.setData({
        status: 1,
        statusChecked: detail
      })
    } else {
      that.setData({
        status: 0,
        statusChecked: detail
      })
    }

  },
  /**
   * 点击保存
   */
  onSaveSubmit: function () {
    var that = this;
    var title = that.data.title;
    var content = that.data.content;
    var level = that.data.levelNum;
    var status = that.data.status;

    //表单验证
    if (test(title, content)) {
      //内容验证并保存
      contentCheck(that, title, content, level, status);
    }
  }
})


/**
 * 表单验证
 * @param {*} title 
 * @param {*} content 
 */
function test(title, content) {
  if (!title) {
    Toast('请输入标题')
    return false;
  } else if (!content) {
    Toast('请输入内容')
    return false;
  } else {
    return true;
  }
}

/**
 * 保存
 * @param {*} title 
 * @param {*} content 
 * @param {*} level 
 * @param {*} status 
 */
function save(that, title, content, level, status) {
  db.collection(app.globalData.memoCollection).add({
    data: {
      "title": title,
      "content": content,
      "level": level,
      "status": status,
      "create_time": utils.getDateTime(new Date())
    }
  }).then(res => {
    // res 是一个对象，其中有 _id 字段标记刚创建的记录的 id
    if (res._id) {
      Dialog.confirm({
        title: '添加成功',
        message: '您已添加成功！您可以选择【继续添加】或者【返回】到上一页',
        confirmButtonText: '继续添加',
        cancelButtonText: '返回'
      })
        .then(() => {
          that.setData({
            title: '',
            content: '',
            levelNum: 0,
            status: 0,
            statusChecked: false
          })
        })
        .catch(() => {
          //返回上一页
          wx.navigateBack({});
        });
    }
  })
    .catch(error => {
      Toast.loading({
        type: 'text',
        duration: 1500,
        message: error.errMsg
      });
    })
}

/**
 * 内容检测
 * @param {} text 
 */
function contentCheck(that, title, content, level, status) {
  wx.cloud.callFunction({
    name: 'ContentCheck',
    data: {
      txt: title + content
    },
    success(res) {
      if (res.result.errCode == 87014) {
        Toast.loading({
          type: 'text',
          duration: 1500,
          message: "存在文字违规，请修改后再保存"
        });
        return false;
      } else {
        //保存
        save(that, title, content, level, status);
      }
    }, fail(err) {
      console.log('ContentCheck-err', err)
      return false;
    }
  })
}