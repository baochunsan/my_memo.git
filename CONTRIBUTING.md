# 项目的贡献指南
1. 首先非常欢迎和感谢对本项目发起Pull Request的码友。
1. **特别提示：请务必在develop分支提交PR，master分支是正式版的代码，即发布正式版本后才会从develop分支进行合并。**
1. **提交代码前，请检查代码是否已经格式化，关键文件、接口、方法等注释清晰，入参、出参描述清楚。**
### 贡献代码步骤
1. 在gitee登录自己的账号，然后fork想要贡献代码的项目。

   ![image-20200827153601786.png](https://i.loli.net/2020/08/27/yfbtl7v5iGsRwIm.png)

2. 在本地新建一个文件夹，然后打开命令行终端，使用 git init 初始化一个新的本地仓库，刚刚新建的文件夹下会出现一个隐藏文件.git。

   ```
   git init
   ```

3. 配置好git的用户名和邮箱，例如：

   ```
   git config --global user.name "xxx"
   git config --global user.email "xxx"
   ```

4. git clone 项目地址，例如：

   ```
   git clone https://gitee.com/baochunsan/my_memo.git
   ```

5. 在克隆下来的项目代码中做修改。因为之前已经做好了修改，所以直接使用对比工具Meld，把自己的修改对比到刚刚克隆下来的项目代码中。全都对比过来后，可以使用git status查看当前项目文件的状态，使用git diff 查看当前项目代码的所有差异。

   ```
   git status
   git diff 
   ```

6. 查看完状态和差异，觉得没有问题后，可以使用git add把修改的文件加入暂存区，为之后的提交做准备。git add . 把所有修改文件加入暂存区。

   ```
   git add .
   ```

7. git commit提交自己的修改。

   ```
   git commit -m "增加了xxx功能，修改了xxxbug"
   ```

8. 把自己在本地的提交推送到gitee上的仓库。因为没有做远程仓库和本地仓库的关联，所以git push origin master之后输入了gitee的账号和密码才能推送到gitee的仓库。推送完成后，可以在fork下来的仓库中查看到刚刚提交的修改了

   ```
   git push origin master
   ```

9. 在 Gitee 网站上提交 Pull Request

![image-20200827154925896.png](https://i.loli.net/2020/08/27/X9WSTjdC8EsViMJ.png)



