# MY备忘 简介

MY备忘，备忘至简，记录习惯！简洁易用的备忘录工具！

MY备忘是基于【微信云开发】进行开发的，为了更加方便快捷的记录而生。

## 基础功能

包含备忘列表【完成/外完成】、搜索备忘、添加备忘、修改备忘、删除备忘、优先级选择以及个人中心展示备忘数统计。

### 功能截图

![image-20200827172721925.png](https://i.loli.net/2020/08/27/qnISKbRDBZQCXmo.png)



![image-20200827172830247.png](https://i.loli.net/2020/08/27/upOnMhl4TyIoeQC.png)

![image-20200827172856519.png](https://i.loli.net/2020/08/27/DQHpYi7BTqJ36Rz.png)

![image-20200827172921382.png](https://i.loli.net/2020/08/27/GNLi4BqP6Z7dl8e.png)

![image-20200827172942288.png](https://i.loli.net/2020/08/27/BOVqoJiSc16CMax.png)

![image-20200827173009981.png](https://i.loli.net/2020/08/27/8xhvX1DATZNbiLw.png)

![image-20200827173026196.png](https://i.loli.net/2020/08/27/3NR4qHrg5ZIp71s.png)

![image-20200827173041803.png](https://i.loli.net/2020/08/27/edDKLV9nyANzoGY.png)

## 部署教程

#### 1、如何下载代码

【gitee官方教程】https://gitee.com/help/articles/4192#article-header0



#### 2、如何将代码导入到开发者工具

- 打开【微信开发者工具】

- 点击【+】号，进入添加小程序页面

  ![image-20200827164131724.png](https://i.loli.net/2020/08/27/VdgFKBtalu4Qzns.png)

- 点击【导入项目】，然后选择目录，定位到从Gitee下载的项目文件夹上

  ![image-20200827164328756.png](https://i.loli.net/2020/08/27/WotR8YjmgFMb6H2.png)

- 点击【导入】按钮完成项目导入

- 

#### 3、哪些参数需要修改

```
无参数修改
```



#### 4、哪些云函数需要部署

- callback【部署】
- echo【部署】
- login【部署】
- openapi【部署】
- timer【暂不需要部署】



#### 5、涉及到的外部服务

```
无外部服务
```



#### 6、云数据库中需要创建哪些数据

- 创建集合
  - memo：备忘录集合
  - user：用户集合



#### 7、云存储中需要上传哪些文件

```
无文件上传
```



#### 8、后台需要配置哪些服务

```
集合的数据权限需要设置为【仅创建者可读写】
```



## License

Apache License Version 2.0

